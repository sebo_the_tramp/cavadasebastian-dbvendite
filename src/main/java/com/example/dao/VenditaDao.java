package com.example.dao;

import com.example.domain.Vendita;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VenditaDao extends JpaRepository<Vendita, Long>{}