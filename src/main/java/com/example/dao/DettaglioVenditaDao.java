package com.example.dao;

import com.example.domain.DettaglioVendita;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DettaglioVenditaDao extends JpaRepository<DettaglioVendita, Long> {}