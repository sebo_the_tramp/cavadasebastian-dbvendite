package com.example.domain;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityReference;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name="dettaglioVendite")

public class DettaglioVendita{
    @Id Long id;
    Integer Riga;
    String Prodotto;
    BigDecimal Importo;
    
    @ManyToOne
    @JoinColumn(name = "ClienteId")
    @JsonIdentityReference
    Cliente cliente;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "NumeroScontrino",nullable = false)
    Vendita vendita;


}