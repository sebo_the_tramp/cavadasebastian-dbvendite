package com.example.domain;

import java.sql.Date;

import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityReference;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name="vendite")

public class Vendita{
    @Id Long NumeroScontrino;
    Date Data;

    @OneToOne(mappedBy = "vendita")
    DettaglioVendita dettaglioVendita;
    
}