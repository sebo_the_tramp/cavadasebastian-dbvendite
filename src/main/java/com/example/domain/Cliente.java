package com.example.domain;

import java.sql.Date;
import java.util.Set;

import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@Table(name="clienti")

public class Cliente{
    @Id Long ClienteId;
    String cognome;
    Integer Age;

    @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
    @JsonIgnore
    Set<DettaglioVendita> dettaglioVendita;
    
}
