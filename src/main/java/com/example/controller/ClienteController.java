package com.example.controller;

import java.util.List;

import com.example.dao.ClienteDao;
import com.example.domain.Cliente;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/clienti", produces = "application/json")
public class ClienteController{
    @Autowired
    private ClienteDao clienteDao;
    @GetMapping(value="")
    public List<Cliente> getClienti(){
        return clienteDao.findAll();
    }
    @GetMapping(value="/id/{idVendita}")
    public Cliente getVendita(@PathVariable Long idCliente) {
        return this.clienteDao.findById(idCliente).get();
    }

    @PostMapping(value="/save")
    public Cliente postMethodName(@RequestBody Cliente Cliente) {
        return this.clienteDao.save(Cliente);
    }
    
    @PutMapping(value="/update")
    public Cliente putMethodName(@RequestBody Cliente Cliente) {
        return this.clienteDao.saveAndFlush(Cliente);
    }
}
