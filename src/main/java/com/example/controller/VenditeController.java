package com.example.controller;

import java.util.List;

import com.example.dao.VenditaDao;
import com.example.domain.Vendita;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/vendite", produces = "application/json")
public class VenditeController {
    @Autowired
    private VenditaDao VenditaDao;
    @GetMapping(value="")
    public List<Vendita> getClienti(){
        return VenditaDao.findAll();
    }
    @GetMapping(value="/id/{idVendita}")
    public Vendita getVendita(@PathVariable Long idVendita) {
        return this.VenditaDao.findById(idVendita).get();
    }

    @PostMapping(value="/save")
    public Vendita postMethodName(@RequestBody Vendita Vendita) {
        return this.VenditaDao.save(Vendita);
    }
    
    @PutMapping(value="/update")
    public Vendita putMethodName(@RequestBody Vendita Vendita) {
        return this.VenditaDao.saveAndFlush(Vendita);
    }
}
