package com.example.controller;

import java.util.List;

import com.example.dao.ClienteDao;
import com.example.dao.DettaglioVenditaDao;
import com.example.domain.Cliente;
import com.example.domain.DettaglioVendita;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/dettaglioVendite", produces = "application/json")
public class DettaglioVenditaController{
    @Autowired
    private DettaglioVenditaDao dettaglioVenditaDao;
    @GetMapping(value="")
    public List<DettaglioVendita> getClienti(){
        return dettaglioVenditaDao.findAll();
    }
    @GetMapping(value="/id/{idVendita}")
    public DettaglioVendita getVendita(@PathVariable Long idVendita) {
        return this.dettaglioVenditaDao.findById(idVendita).get();
    }

    @PostMapping(value="/save")
    public DettaglioVendita postMethodName(@RequestBody DettaglioVendita Vendita) {
        return this.dettaglioVenditaDao.save(Vendita);
    }
    
    @PutMapping(value="/update")
    public DettaglioVendita putMethodName(@RequestBody DettaglioVendita Vendita) {
        return this.dettaglioVenditaDao.saveAndFlush(Vendita);
    }
}
